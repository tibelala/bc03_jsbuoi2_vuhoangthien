/**
 * Input: 45
 * Hàng đơn vị: input x 10%
 * Hàng chục: input/10
 * output: hàng chục + hàng đơn vị
 */

var ketQua = function () {
  var sotuNhienvalue = document.getElementById("txt_so_tu_nhien").value * 1;
  var hangdonvi = sotuNhienvalue % 10;
  var hangchuc = sotuNhienvalue / 10;
  var ketqua = hangchuc + hangdonvi;
  document.getElementById(
    "ket_qua"
  ).innerHTML = `Kết quả = ${ketqua} <br> Hàng chục = ${hangchuc} <br> Hàng đơn vị = ${hangdonvi}`;
};

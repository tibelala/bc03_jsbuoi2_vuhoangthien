/**
 * Input: dài 4cm, rộng 5cm
 * - Chu vi: (4+5)*2
 * - Diện tích: 4*5
 * Output: 18cm, 20cm
 */

var ketQua = function () {
  var chieuDaivalue = document.getElementById("txt_chieu_dai").value * 1;
  var chieuRongvalue = document.getElementById("txt_chieu_rong").value * 1;
  var dientich = chieuDaivalue * chieuRongvalue;
  var chuvi = (chieuDaivalue + chieuRongvalue) * 2;
  document.getElementById(
    "ket_qua"
  ).innerHTML = `Diện tích = ${dientich} <br> Chu vi = ${chuvi}`;
};

/**
 * Input: 5 số thực là: 1,2,3,4,5
 * Output: (1+2+3+4+5)/5
 */

var tinhtb = function () {
  var sothuNhatvalue = document.getElementById("txt_so_thu_nhat").value * 1;
  var sothuhaivalue = document.getElementById("txt_so_thu_hai").value * 1;
  var sothubavalue = document.getElementById("txt_so_thu_ba").value * 1;
  var sothutuvalue = document.getElementById("txt_so_thu_tu").value * 1;
  var sothunamvalue = document.getElementById("txt_so_thu_nam").value * 1;
  var ketqua =
    (sothuNhatvalue +
      sothuhaivalue +
      sothubavalue +
      sothutuvalue +
      sothunamvalue) /
    5;
  document.getElementById("ket_qua").innerHTML = `<p>Kết quả: ${ketqua}</p>`;
};
